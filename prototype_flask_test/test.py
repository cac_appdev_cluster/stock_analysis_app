from flask import Flask, redirect, url_for, render_template, request
from yahoo_fin import stock_info as si


app = Flask(__name__)


@app.route("/", methods=["POST","GET"])
def home():
    if request.method == "POST":
        stock = request.form["nm"]
        return redirect(url_for("stock", stk=stock))
    else:    
        return render_template("login.html")

@app.route("/<stk>")
def stock(stk):
    price = si.get_live_price(stk)
    return f"<h1><p>Stock: {stk} </p>Price: {round(price,2)}</h1>"

if __name__=="__main__":
    app.run(debug=True)